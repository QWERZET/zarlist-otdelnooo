from .. import loader, utils
import asyncio, pytz, re, telethon
from telethon.tl.types import MessageEntityTextUrl
import json as JSON
from datetime import datetime, date, time

class Zarlist (loader.Module):
	"Пусть будет отдельно ."
	strings={"name": "Zarlist"}
	
	async def client_ready(self, client, db):
		self.db = db
		if not self.db.get("Zarlist", "exUsers", False):
			self.db.set("Zarlist", "exUsers", [])
		if not self.db.get("Zarlist", "", False):
			self.db.set("Zarlist", "infList", {})
		
	async def zarlistcmd(self, message):
		""" Лист ваших заражений.\nИспользуй: .zarlist (@id/user) (цифра) (аргумент)\nДля удаления: .zarlist (@id/user)\nАргументы:\n-k — Добавить тысячу к числу.\n-f — Поиск по @id/user.\n-r — Добавляет в список по реплаю."""
		args = utils.get_args_raw(message)
		infList = self.db.get("Zarlist", "infList")
		timezone = "Europe/Kiev"
		vremya = datetime.now(pytz.timezone(timezone)).strftime("%d.%m")
		try:
			args_list = args.split(' ')
		except:
			pass
		if not args:
			if not infList:
				await utils.answer(message, "Лист заражений <b>пуст</b>❗️")
				return
			sms = ''
			for key, value in infList.items():
				sms+=f'<b>• <code>{key}</code> — <code>{value[0]}</code> [<i>{value[1]}</i>]</b>\n'
			await utils.answer(message, sms)
			return
		if not '-r' in args.lower():
			if args_list[0] == "clear":
				infList.clear()
				self.db.set("Zarlist", "infList", infList)
				await utils.answer(message, "Лист заражений <b>очищен</b>.")
			elif args_list[0] in infList and '-f' in args.lower():
				user = infList[args_list[0]]
				await utils.answer(message, f"<b>• <code>{args_list[0]}</code> — <code>{user[0]}</code> [<i>{user[1]}</i>]</b>")
				await asyncio.sleep(5)
			elif len(args_list) == 1 and args_list[0] in infList:
				infList.pop(args_list[0])
				self.db.set("Zarlist", "infList", infList)
				await utils.answer(message, f"<b>Пользователь</b> <code>{args}</code> <b>удалён из списка.</b>")
				await asyncio.sleep(3)
			elif args_list[0][0] != '@':
				await utils.answer(message, '<b>Это не</b> <code>@id/user</code>❗️')
			else:
				try:
					user, count = str(args_list[0]), float(args_list[1])
				except:
					await utils.answer(message, "<b>Не правильный аргумент либо этого @id нету в списке❗️</b>")
					return
				k = ''
				if '-k' in args.lower():
					k+='k'
				infList[user] = [str(count)+k, vremya]
				self.db.set("Zarlist", "infList", infList)
				await utils.answer(message, f"Пользователь <code>{user}</code> добавлен в список заражений.\nЧисло: <code>{count}</code>{k}\nДата: <b>{vremya}</b>")
				await asyncio.sleep(5)
			await message.delete()
		else:
			reply = await message.get_reply_message()
			if not reply: 
				return await utils.answer(message, 'Реплай должен быть на смс ириса "<b>...подверг заражению...</b>"❗️')
			elif reply.sender_id != 707693258 and not 'подверг заражению' in reply.text:
				return await utils.answer(message, 'Реплай должен быть на смс ириса "<b>...подверг заражению...</b>"❗️')
			else: #☣
				text = reply.text
				x = text.index('☣')+4
				count = text[x:].split(' ', maxsplit=1)[0]
				x = text.index('user?id=') + 8
				user = '@' + text[x:].split('"', maxsplit=1)[0]
				infList[user] = [str(count), vremya]
				self.db.set("Zarlist", "infList", infList)
				await utils.answer(message, f"<b>Пользователь</b> <code>{user}</code> <b>добавлен в список заражений.</b>\n<b>Число</b>: <code>{count}</code>\n<b>Дата</b>: <b>{vremya}</b>")
